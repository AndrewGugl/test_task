CREATE TABLE users (
	id INTEGER NOT NULL, 
	username VARCHAR(250), 
	pwdhash VARCHAR, 
	is_staff BOOLEAN, 
	PRIMARY KEY (id), 
	UNIQUE (username), 
	CHECK (is_staff IN (0, 1))
);
;
CREATE TABLE authors (
	id INTEGER NOT NULL, 
	name VARCHAR(80), 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
;
CREATE TABLE books (
	id INTEGER NOT NULL, 
	title VARCHAR(250), 
	PRIMARY KEY (id), 
	UNIQUE (title)
);
;
CREATE TABLE authors_books (
	author_id INTEGER, 
	book_id INTEGER, 
	FOREIGN KEY(author_id) REFERENCES authors (id), 
	FOREIGN KEY(book_id) REFERENCES books (id)
);
