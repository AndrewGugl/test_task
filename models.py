from sqlalchemy import Table, Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship, backref
from database import Base
from werkzeug import generate_password_hash, check_password_hash


authors_books = Table(
    'authors_books', Base.metadata,
    Column('author_id', Integer, ForeignKey('authors.id')),
    Column('book_id', Integer, ForeignKey('books.id'))
    )


class User(Base):
    """
    User of library
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(250), unique=True)
    pwdhash = Column(String())
    is_staff = Column(Boolean)

    def __init__(self, username, password, is_staff=False):
        self.username = username
        self.pwdhash = generate_password_hash(password)
        if is_staff:
            self.is_staff = True

    def __repr__(self):
        return '<Username %r>' % self.username

    def check_password(self, password):
        return check_password_hash(self.pwdhash, password)


class Book(Base):
    """
    Book of library
    """
    __tablename__ = 'books'
    id = Column(Integer, primary_key=True)
    title = Column(String(250), unique=True)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return '<Book %r>' % self.title


class Author(Base):
    """
    Author of book
    """
    __tablename__ = 'authors'
    id = Column(Integer, primary_key=True)
    name = Column(String(80), unique=True)
    books = relationship('Book', secondary=authors_books,
                         backref=backref('authors', lazy='dynamic'))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Author %r>' % self.name
