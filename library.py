from functools import wraps
from flask import Flask, render_template, session, request, make_response,\
    redirect, url_for
from database import db_session
from flask.ext.script import Manager
from forms import RegistrationForm, AddAuthorForm, AddBookForm, EditBookForm
from models import User, Author, Book
from database import init_db


app = Flask(__name__)
app.config.from_object(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///library.db'
manager = Manager(app)


@manager.command
def init():
    """
    Create library.db (sqlite).
    """
    init_db()


@manager.command
def fake_data():
    """
    Load fake initial data to library.db .
    """
    pass


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('username', None) is None:
            return redirect(url_for('login'), next=request.url)
        return f(*args, **kwargs)
    return decorated_function


def staff_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user = db_session.query(User).filter(User.username ==
                                             session['username']).first()
        if not user.is_staff:
            error = "You don't have permission to edit"
            return render_template('index.html', error=error)
        return f(*args, **kwargs)
    return decorated_function


@app.route('/')
def index():
    """
    Main page
    """
    error = None
    return render_template('index.html', error=error)


def check_login(username, password):
    """
    Authentification of user
    """
    query = db_session.query(User).filter(User.username == username)
    user = query.first()
    if not user:
        return 'Invalid username'
    if not user.check_password(password):
        return 'Invalid password'


@app.route('/register', methods=['GET', 'POST'])
def register():
    """
    Registration of user
    """
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.username.data,
                    form.password.data,
                    form.is_staff.data)
        db_session.add(user)
        db_session.flush()
        return redirect(url_for('login'))
    return render_template('registration.html', form=form)


@app.route('/login', methods=['POST', 'GET'])
def login():
    """
    Login page
    """
    error = None
    if request.method == 'POST':
        error = check_login(request.form['username'], request.form['password'])
        if not error:
            session['username'] = request.form['username']
            return make_response(render_template('index.html'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    """
    User logout
    """
    # remove the username from the session if it's there
    error = None
    session.pop('username', None)
    return render_template('index.html', error=error)


def is_staff(username):
    """
    Return 'True' i f user is staff
    """
    user = db_session.query(User).filter(User.username == username).first()
    return user.is_staff


@app.route('/books', methods=['GET', 'POST'])
def books():
    """
    Return page with list of books.
    Search by book or by author
    """
    error = None
    staff = False
    books = []
    if session.get('username', None):
        staff = is_staff(session['username'])
        if request.args.get('book', ''):
            books = db_session.query(Book).filter(Book.title.like('%' +
                request.args['book'] + '%')).all()
        elif request.args.get('author', ''):
            books = db_session.query(Book).filter(Book.authors.any(Author.name.like('%'
                + request.args.get('author', '') + '%'))).all()
        else:
            books = Book.query.all()
    else:
        if request.args.get('book', '') or request.args.get('author', ''):
            error='You have not permission. Login first'
        books = Book.query.all()

    return render_template('books.html', error=error, books=books,
                           staff=staff)


@app.route('/authors')
def authors():
    """
    Return page with list of authors.
    """
    error = None
    authors = Author.query.all()
    staff = False
    if session.get('username', None):
        staff = is_staff(session['username'])
    return render_template('authors.html', error=error, authors=authors,
                           staff=staff)


@app.route('/authors/add', methods=['GET', 'POST'])
@login_required
@staff_required
def add_author():
    form = AddAuthorForm(request.form)
    if request.method == 'POST' and form.validate():
        author = Author(form.name.data)
        db_session.add(author)
        db_session.flush()
        return redirect(url_for('authors'))
    return render_template('add_author.html', form=form)


@app.route('/books/add', methods=['GET', 'POST'])
@login_required
@staff_required
def add_book():
    """
    Return page with add book form
    """
    form = AddBookForm(request.form)
    form.authors.choices = [(str(a.id), a.name) for a in Author.query.order_by('name')]
    if request.method == 'POST' and form.validate():
        book = Book(form.title.data)
        if form.authors.data:
            for author_id in form.authors.data:
                author = Author.query.get(author_id)
                book.authors.append(author)
        db_session.add(book)
        db_session.flush()
        return redirect(url_for('books'))
    return render_template('add_book.html', form=form)


@app.route('/books/edit/<int:book_id>', methods=['GET', 'POST'])
@login_required
@staff_required
def edit_book(book_id):
    """
    Return page with edit book form
    """
    book = Book.query.get(book_id)
    form = EditBookForm(request.form, obj=book)
    form.authors.choices = [(str(a.id), a.name) for a in Author.query.\
                                                            order_by('name')]
    # form.authors.default = [str(b.id) for b in book.authors.all()]
    if request.method == 'POST' and form.validate():
        db_session.add(book)
        if book.title != form.title.data:
            db_session.query(Book).filter(Book.id == book_id).\
                update({Book.title: form.title.data})

        # list of book.authors id
        book_authors = []
        for author in book.authors.all():
            book_authors.append(author.id)

        # remove unselected authors
        for author_id in book_authors:
            if author_id not in form.authors.data:
                # delete b

                author = book.authors.filter(Author.id == author_id).first()
                book.authors.remove(author)

        # add new authors
        for author_id in form.authors.data:
            if author_id not in book_authors:
                author = Author.query.get(author_id)
                book.authors.append(author)

        db_session.flush()
        return redirect(url_for('books'))
    return render_template('edit_book.html', form=form, book=book)


@app.route('/authors/edit/<int:author_id>', methods=['GET', 'POST'])
@login_required
@staff_required
def edit_author(author_id):
    """
    Return page with edit author form
    """
    author = Author.query.get(author_id)
    form = AddAuthorForm(request.form, obj=author)
    if request.method == 'POST' and form.validate():
        if author.name != form.name.data:
            db_session.query(Author).filter(Author.id == author_id).\
                update({Author.name: form.name.data})
        return redirect(url_for('authors'))
    return render_template('edit_author.html', form=form, author=author)


@app.route('/books/remove/<int:book_id>', methods=['GET', 'POST'])
@login_required
@staff_required
def remove_book(book_id):
    """
    Remove book
    """
    book = Book.query.get(book_id)
    if book:
        db_session.delete(book)
        db_session.flush()
    return redirect(url_for('books'))


@app.route('/authors/remove/<int:author_id>', methods=['GET', 'POST'])
@login_required
@staff_required
def remove_author(author_id):
    """
    Remove author
    """
    author = Author.query.get(author_id)
    if author:
        db_session.delete(author)
        db_session.flush()
    return redirect(url_for('authors'))


app.secret_key = 'sdflaSFSDA0ZrR~XHH!jmN]LWX/,?RT'
if __name__ == '__main__':
    app.debug = True
    manager.run()
