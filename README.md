## This is a test tasks

### Installation

1. You have to clone repo


2. Create environment and activate it:

    virtualenv .env
    . .env/bin/activate


3. Install other packages

    pip install -r requirements.txt


4. Run web development server:

    python library.py runserver   (127.0.0.1:5000)


5. If you want create new database, delete library.db,
    and run command:

    python library.py init
