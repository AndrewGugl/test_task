import os
import unittest
import tempfile
import library
from models import User
from database import db_session
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///library-test.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    """
    Create database
    """
    import models
    Base.metadata.create_all(bind=engine)


class LibraryTestCase(unittest.TestCase):

    def setUp(self):
        """
        Before each test, set up a blank database
        """
        self.db_fd, library.app.config['DATABASE'] =\
                                         tempfile.mkstemp()

        library.app.config['TESTING'] = True
        library.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///library-test.db'
        self.app = library.app.test_client()
        init_db()


    def tearDown(self):
        """
        Get rid of the database again after each test.
        """
        os.close(self.db_fd)
        os.unlink(library.app.config['DATABASE'])

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    # testing functions

    def test_login_logout(self):
        """
        Make sure login and logout works
        """
        username = 'Andrew'
        password = '12345678'
        user = User(username, password)

        r = self.login(username,
                       password)
        assert b'Hello, guys' in r.data
        r = self.logout()
        assert b'Anonymous' in r.data
        r = self.login(username + 'x',
                       password)
        assert b'Invalid username' in r.data
        r = self.login(username,
                       password + 'x')
        assert b'Invalid password' in r.data

    def test_urls(self):
        """
        Test response =='ok'
        """
        r = self.app.get('/')
        self.assertEquals(r.status_code, 200)

        r = self.app.get('/login')
        self.assertEquals(r.status_code, 200)

        r = self.app.get('/logout')
        self.assertEquals(r.status_code, 200)

        r = self.app.get('/books')
        self.assertEquals(r.status_code, 200)

        r = self.app.get('/authors')
        self.assertEquals(r.status_code, 200)


def run_tests():
    unittest.main()


if __name__ == '__main__':
    unittest.main()
