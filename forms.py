from wtforms import Form, TextField, PasswordField, validators, BooleanField,\
    SelectMultipleField
from wtforms.validators import ValidationError
from models import Author


class RegistrationForm(Form):
    """
    New User registration form
    """
    username = TextField('Username', [validators.Length(min=4, max=250)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    is_staff = BooleanField('Is staff?', [validators.AnyOf([False, True])])


class AddBookForm(Form):
    """
    New Book form
    """
    title = TextField('Title', [validators.Length(min=4, max=250)])
    authors = SelectMultipleField('Authors')


class EditBookForm(Form):
    """
    Edit Book form
    """
    title = TextField('Title', [validators.Length(min=4, max=250)])
    authors = SelectMultipleField('Authors')

    def validate_auhtors(form, field):
        for author_id in field.data:
            if not Author.query.get(author_id):
                raise ValidationError('Author is not exists')


class AddAuthorForm(Form):
    """
    New Author form
    """
    name = TextField('Name', [validators.Length(min=4, max=80)])
