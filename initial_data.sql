PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE users (
	id INTEGER NOT NULL, 
	username VARCHAR(250), 
	pwdhash VARCHAR, 
	is_staff BOOLEAN, 
	PRIMARY KEY (id), 
	UNIQUE (username), 
	CHECK (is_staff IN (0, 1))
);
INSERT INTO "users" VALUES(1,'admin','pbkdf2:sha1:1000$LQZcIrYR$89bdbae79dbe030ef23cffbbdad870d8810a77e5',1);
INSERT INTO "users" VALUES(2,'Andrew','pbkdf2:sha1:1000$dNt2H5Zg$4a0ab17fc8671c92f3a4f322c88dd64d77e50b23',NULL);
CREATE TABLE authors (
	id INTEGER NOT NULL, 
	name VARCHAR(80), 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
INSERT INTO "authors" VALUES(1,'Геродот');
INSERT INTO "authors" VALUES(2,'Плутарх');
INSERT INTO "authors" VALUES(3,'Платон');
INSERT INTO "authors" VALUES(4,'Аристотель');
INSERT INTO "authors" VALUES(5,'Евклид');
INSERT INTO "authors" VALUES(6,'Плиний');
INSERT INTO "authors" VALUES(7,'Лукреций');
INSERT INTO "authors" VALUES(8,'Марк Аврелий');
CREATE TABLE books (
	id INTEGER NOT NULL, 
	title VARCHAR(250), 
	PRIMARY KEY (id), 
	UNIQUE (title)
);
INSERT INTO "books" VALUES(1,'История');
INSERT INTO "books" VALUES(2,'Сравинтельное жизнеописание');
INSERT INTO "books" VALUES(3,'Диалоги');
INSERT INTO "books" VALUES(4,'Метафизика');
INSERT INTO "books" VALUES(5,'Начала');
INSERT INTO "books" VALUES(6,'Естественная истоия');
INSERT INTO "books" VALUES(7,'О природе вещей');
INSERT INTO "books" VALUES(8,'Наедине с сабой');
CREATE TABLE authors_books (
	author_id INTEGER, 
	book_id INTEGER, 
	FOREIGN KEY(author_id) REFERENCES authors (id), 
	FOREIGN KEY(book_id) REFERENCES books (id)
);
INSERT INTO "authors_books" VALUES(1,1);
INSERT INTO "authors_books" VALUES(2,2);
INSERT INTO "authors_books" VALUES(3,3);
INSERT INTO "authors_books" VALUES(4,4);
INSERT INTO "authors_books" VALUES(5,5);
INSERT INTO "authors_books" VALUES(6,6);
INSERT INTO "authors_books" VALUES(7,7);
INSERT INTO "authors_books" VALUES(4,8);
INSERT INTO "authors_books" VALUES(1,8);
INSERT INTO "authors_books" VALUES(5,8);
INSERT INTO "authors_books" VALUES(7,8);
COMMIT;
